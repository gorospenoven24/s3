<?php 
class Person { 
    public $firstName;
    public $middleName;
    public $lastName;

    public function __construct($firstName, $middleName, $lastName) {
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }
    public function printName() {
        return "Your Fullname is  $this->firstName  $this->lastName";
    }
}

class Developer extends Person {
    public function printName() {
        return "Your name is $this->firstName $this->middleName $this->lastName and you are developer";
    }
}

class Engineer extends Person {
    public function printName() {
        return "Your are an Engineer name $this->firstName $this->middleName $this->lastName and you are developer";
    }
}

$person = new Person('Senku', ' ' , 'Ishigami');
$developer = new Developer('John', 'Finch' , 'Smith');
$engineer = new Engineer('Harold', 'Myers' , 'Reese');
// $fullName = $person

